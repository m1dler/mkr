<?php


class Trener
{   public $trener;
    public $nationality;
    public $wins;
    /**
     * @return mixed
     */
    public function getTrener()
    {
        return $this->trener;
    }

    /**
     * @param mixed $trener
     */
    public function setTrener($trener)
    {
        $this->trener = $trener;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return mixed
     */
    public function getWins()
    {
        return $this->wins;
    }

    /**
     * @param mixed $wins
     */
    public function setWins($wins)
    {
        $this->wins = $wins;
    }

}
